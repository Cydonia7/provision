import pacman
import os
import configparser
from helpers import *

@section('Update all packages')
def update_packages(context):
    return pacman.update_all_packages(context)

@section('Install core packages')
def core_packages(context):
    pacman.remove_if_needed(context, ['vi'])
    return pacman.install_or_update_if_needed(context, ['xclip', 'yay', 'ripgrep', 'alacritty', 'zsh', 'fish', 'arc-gtk-theme', 'arc-solid-gtk-theme', 'arc-icon-theme', 'emacs', 'rofi', 'noto-fonts-emoji', 'thunderbird'])

@section('Install AUR packages')
def aur_packages(context):
    return pacman.install_or_update_if_needed(context, ['google-chrome', 'ondir-git', 'neovim', 'neovim-symlinks', 'nomachine', 'starship-bin'], aur=True)

@section('Install various config files')
def install_config(context):
    if not runs_correctly(context, 'cat config.ini > /dev/null'):
        context.run('cp config.ini.dist config.ini')

    config = configparser.ConfigParser()
    config.read('config.ini')
    monospace_font = config['fonts']['monospace']
    alacritty_font_size = config['alacritty']['font_size']
    emacs_font_size = config['emacs']['font_size']

    context.run('mkdir -p ~/.config/alacritty')
    context.run('mkdir -p ~/.config/nvim')
    context.run('mkdir -p ~/.config/rofi')
    context.run('sed -e "s/\[alacritty\.font_size\]/%s/g" -e "s/\[fonts\.monospace\]/%s/g" files/alacritty.yml > ~/.config/alacritty/alacritty.yml' % (alacritty_font_size, monospace_font))
    context.run('cp files/ondirrc ~/.ondirrc')
    context.run('cp files/nvimrc ~/.config/nvim/init.vim')
    context.run('cp files/tmux.conf ~/.tmux.conf')
    context.run('cp files/gitignore ~/.gitignore')
    context.run('cp files/gitconfig ~/.gitconfig')
    context.run('cp files/rofi ~/.config/rofi/config')
    context.run('cp files/ssh ~/.ssh/config')
    context.run('rm -rf ~/.config/fish')
    context.run('cp -r files/fish ~/.config/fish')
    context.sudo('chsh -s /usr/bin/fish $(logname)')
    context.run('cp files/starship ~/.config/starship.toml')
    context.run('sed -e "s/\[emacs\.font_size\]/%s/g" -e "s/\[fonts\.monospace\]/%s/g" files/spacemacs > ~/.spacemacs' % (emacs_font_size, monospace_font))

def single_font(name, url, filename):
    @section('Install %s' % name)
    def callback(context):
        if runs_correctly(context, 'fc-list | grep "%s"' % name):
            return 'skipped'

        context.run('mkdir -p ~/.local/share/fonts')
        context.run('curl -L %s -o ~/.local/share/fonts/%s' % (url, filename))

    return callback

def fonts_in_zip(name, url, fonts_glob):
    @section('Install %s family' % name)
    def callback(context):
        if runs_correctly(context, 'fc-list | grep "%s"' % name):
            return 'skipped'

        context.run('mkdir -p ~/.local/share/fonts')
        context.run('rm -f /tmp/fonts.zip')
        context.run('curl -L %s -o /tmp/fonts.zip' % url)
        context.run('unzip -j /tmp/fonts.zip \'%s\' -d ~/.local/share/fonts' % fonts_glob)
        #context.run('rm ~/tmp/fonts.zip')

    return callback

@section('Install fonts', subsections=[
    single_font('SF Mono', 'https://github.com/ZulwiyozaPutra/SF-Mono-Font/raw/master/SFMono-Medium.otf', 'sf-mono.otf'),
    single_font('SF UI Display', 'https://github.com/squid-app/desktop/raw/master/src/fonts/SF-UI-Display-Regular.ttf', 'sf-ui-display.ttf'),
    single_font('Cascadia Code', 'https://github.com/microsoft/cascadia-code/releases/download/v1911.21/Cascadia.ttf', 'cascadia-code.ttf'),
    fonts_in_zip('JetBrains Mono', 'https://download.jetbrains.com/fonts/JetBrainsMono-1.0.2.zip?_ga=2.250792774.1909519080.1580686664-442757449.1580686664', 'JetBrains*/ttf/*.ttf'),
    fonts_in_zip('Inter', 'https://github.com/rsms/inter/releases/download/v3.12/Inter-3.12.zip', 'Inter/*.otf'),
])
def fonts(context):
    context.run('fc-cache -v')

@section('Install Rust')
def rustup(context):
    if already_installed(context, '~/.cargo/bin/rustc'):
        return 'skipped'

    context.run("curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y")
    context.run('~/.cargo/bin/rustup override set stable')
    context.run('~/.cargo/bin/rustup update stable')

@section('Install and configure DNSMasq')
def dnsmasq(context):
    context.sudo('cp files/dnsmasq.conf /etc/dnsmasq.conf')

    if runs_correctly(context, 'systemctl status dnsmasq | grep running'):
        return 'skipped'

    pacman.install_or_update_if_needed(context, ['dnsmasq'])
    context.sudo('chattr -i /etc/resolv.conf')
    context.sudo('sh -c "echo nameserver 127.0.0.1 > /etc/resolv.conf"')
    context.sudo('chattr +i /etc/resolv.conf')
    context.sudo('systemctl enable dnsmasq')
    context.sudo('systemctl restart dnsmasq')
    context.sudo('systemctl restart NetworkManager')

@section('Install Docker')
def docker(context):
    if runs_correctly(context, 'docker info'):
        return 'skipped'

    pacman.install_or_update_if_needed(context, ['docker', 'docker-compose'])
    context.sudo('systemctl enable docker')
    context.sudo('systemctl restart docker')
    context.sudo('gpasswd -a $USER docker')

@section('Install fzf')
def fzf(context):
    if runs_correctly(context, 'ls ~/.fzf'):
        return 'skipped'

    context.run('git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf')
    context.run('~/.fzf/install --all')
    context.run('cp files/zshrc ~/.zshrc')

@section('Install Spacemacs')
def spacemacs(context):
    if runs_correctly(context, 'ls ~/.emacs.d'):
        return 'skipped'

    context.run('git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d')

def next_steps(context):
    print()
    print('Still left to do manually:')   
    print('- Set theme in Appearance to Arc-Darker and icon theme to Arc, font to Inter Medium & Jetbrains Mono Medium')
    print('- Log in to your Pcloud account')
    print('- Add Bitwarden to Chrome and log in')
    print('- Set up the favorites menu with Alacritty and Chrome')   
    print('- Set up a nice wallpaper and change the default start menu icon to /usr/share/icons/Arc/apps/32/unity-color-panel.png')
    print('- Set up a keyboard shortcut for "rofi -show drun"')
    print('- Install the font gist')
    print()

@section('Provision computer', subsections=[install_config, update_packages, core_packages, aur_packages, fonts, rustup, dnsmasq, docker, fzf, spacemacs, next_steps])
def install(context):
    pass

def github_clone(context, organization, repo):
    add_to_known_hosts(context, 'github.com')
    return git_clone(context, 'https://github.com/%s/%s.git' % (organization, repo), repo)

def git_clone(context, url, name):
    with context.cd('/tmp'):
        context.run('rm -rf /tmp/%s' % name)
        context.run('git clone %s %s' % (url, name))

    return '/tmp/%s' % name

def add_to_known_hosts(context, domain):
    if runs_correctly(context, 'cat ~/.ssh/known_hosts | grep %s' % domain):
        return

    context.run('ssh-keyscan %s >> ~/.ssh/known_hosts' % domain)

def already_installed(context, command):
    return runs_correctly(context, 'which %s' % command)

def runs_correctly(context, command, sudo=False):
    if sudo:
        return context.sudo(command, warn=True).exited == 0

    return context.run(command, warn=True).exited == 0

