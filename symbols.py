def symbol(symbol, color):
    return "\033[3%dm%s\033[0m" % (color, symbol)

red_cross = symbol('✖', 1)
green_tick = symbol('✔', 2)
fat_yellow_arrow = symbol('❯', 3)
blue_arrow = symbol('→', 4)

