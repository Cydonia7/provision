from invoke import task, exceptions
from logger import logger
import functools

def section(title, subsections = []):
    def decorator(function):
        @functools.wraps(function)
        @task(incrementable=['verbose'])
        def wrapper(context, verbose=0, *args, **kwargs):
            logger.set_verbosity_if_not_set(verbose)
            logger.begin_step(title)
            try:
                for subsection in subsections:
                    subsection(context, *args, **kwargs)
                result = function(Context(context), *args, **kwargs)
                if result == 'skipped':
                    logger.step_skipped()
                else:
                    logger.step_success()
            except exceptions.UnexpectedExit as err:
                logger.step_failure(err)
        return wrapper
    return decorator

class Context:
    def __init__(self, context):
        self.context = context

    def run(self, command, **kwargs):
        logger.command(command)
        return self.context.run(command, hide=logger.should_hide_command_output(), **kwargs)

    def sudo(self, command, **kwargs):
        logger.command(command)
        return self.context.sudo(command, hide=logger.should_hide_command_output(), **kwargs)

    def cd(self, path):
        return self.context.cd(path)
