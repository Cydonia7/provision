# *-* coding: utf-8 *-*

import sys
import time
import os
from symbols import *

class Logger:
    def __init__(self):
        self.verbosity = 0
        self.section_level = 0
        self.timers = []
        self.has_set_verbosity = False

    def begin_step(self, title):
        self.title = title
        self.section_level += 1
        self.timers.append(time.perf_counter())
        self.print_at_section_level(fat_yellow_arrow, title)

    def step_success(self):
        duration = time.perf_counter() - self.timers.pop()
        if self.is_info():
            self.print_inside_section(blue_arrow, 'Finished successfully in %.3fs' % duration)
        elif self.title:
            self.replace_current_line(green_tick, '%s \033[30;1m(%.3fs)' % (self.title, duration))
            self.title = None
        self.section_level -= 1

    def step_skipped(self):
        duration = time.perf_counter() - self.timers.pop()
        if self.is_info():
            self.print_inside_section(blue_arrow, 'Skipped because it is already installed, section took %.3fs' % duration)
        elif self.title:
            self.replace_current_line(green_tick, "%s \033[30;1m(skipped, %.3fs)" % (self.title, duration))
            self.title = None
        self.section_level -= 1

    def step_failure(self, error):
        stdout, stderr = error.streams_for_display()
        self.print_inside_section(red_cross, 'The previous command returned exit code %d' % error.result.exited)
        self.print_inside_section(red_cross, 'Stdout: ' + "\n" + stdout.strip())
        self.print_inside_section(red_cross, 'Stderr: ' + "\n" + stderr.strip())
        sys.exit(1)

    def command(self, command):
        if self.is_info():
            self.print_inside_section(blue_arrow, command)

    def print_at_section_level(self, symbol, message):
        sys.stdout.write("%s%s %s\033[0m\n" % (self.current_indent(), symbol, message))

    def print_inside_section(self, symbol, message):
        self.section_level += 1
        self.print_at_section_level(symbol, message)
        self.section_level -= 1

    def replace_current_line(self, symbol, message):
        sys.stdout.write("%s\033[A\033[1K%s %s\033[0m\n" % (self.current_indent(), symbol, message))

    def should_hide_command_output(self):
        return not self.is_debug()

    def current_indent(self):
        return '  ' * (self.section_level - 1)

    def set_verbosity_if_not_set(self, verbosity):
        if self.has_set_verbosity: # Only the main task can set verbosity
            return

        self.verbosity = verbosity
        self.has_set_verbosity = True

    def is_quiet(self):
        return self.verbosity == 0

    def is_info(self, strict=False):
        return (strict and self.verbosity == 1) or self.verbosity >= 1

    def is_debug(self, strict=False):
        return (strict and self.verbosity == 2) or self.verbosity >= 2

logger = Logger()

