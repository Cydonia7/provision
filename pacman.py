from logger import logger

def generate_mirrors_if_needed(context):
    if int(context.run('find /etc/pacman.d -mtime -30 -type f -name mirrorlist 2> /dev/null | wc -l').stdout) == 0:
        context.sudo('pacman-mirrors -g --country France')

def remove_if_needed(context, packages):
    context.sudo('pacman -Rscn --noconfirm %s' % ' '.join(packages), warn=True)

def install_or_update_if_needed(context, packages, aur=False):
    generate_mirrors_if_needed(context)

    program = 'yay' if aur else 'pacman'
    context.sudo('sed -i s/#Color/Color/g /etc/pacman.conf')
    context.sudo(program + ' -Sy')
    results = context.run(program + ' -Qu ' + ' '.join(packages), warn=True)
    to_upgrade = ''
    
    if results.stdout:
        for upgrade_found in results.stdout.rstrip().split("\n"):
            to_upgrade += upgrade_found.split()[0] + ' '

    results = context.run("echo %s | xargs -n 1 sh -c '%s -Qi $0 >/dev/null 2>&1; echo $?'" % (' '.join(packages), program))
    to_upgrade += ' '.join([x for (x, y) in zip(packages, results.stdout.split("\n")) if y == '1'])

    if not to_upgrade.strip():
        return 'skipped'

    if aur:
        context.run(program + ' -S --noconfirm ' + to_upgrade)
    else:
        context.sudo(program + ' -S --noconfirm ' + to_upgrade)

def update_all_packages(context):
    generate_mirrors_if_needed(context)

    if 'there is nothing to do' in context.sudo('pacman -Syu --noconfirm').stdout:
        return 'skipped'

